﻿// Write your JavaScript code.
$.getJSON("https://api.coinmarketcap.com/v1/ticker/?convert=NZD&limit=33", function (json) {
    for (i = 0; i < json.length; i++) {
        $("#name").append(json[i].name + "<br>");
        $("#symbol").append(json[i].symbol + "<br>");
        $("#rank").append(json[i].rank + "<br>");
        $("#pricenzd").append(json[i].price_nzd + "<br>");
        $("#pricebtc").append(json[i].price_btc + "<br>");
        $("#marketcap").append(json[i].market_cap_nzd + "<br>");
        $("#totalsupply").append(json[i].total_supply + "<br>");
        $("#purchase").append("<button>Buy</button><input type='text'><br>");
        $("#sell").append("<button>Sell</button><input type='text'><br>");
    };
});

var counter = 1;

function ShowMarketHomePage() {

    if (counter % 2 == 1)
    {
        $('#popup').fadeIn();
        document.getElementById('firstpagemarketinfo').innerHTML = "Close Market Info";
        counter++;
        setTimeout(function () {
            document.getElementById('myCarousel').style.display = "none";
        }, 500);
    }
    else
    {
        $('#popup').fadeOut();
        document.getElementById('firstpagemarketinfo').innerHTML = "See Current Market";
        document.getElementById('myCarousel').style.display = "block";
        counter++;
    }
}